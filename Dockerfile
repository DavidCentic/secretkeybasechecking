FROM ruby:2.5.1
RUN apt-get update -qq && apt-get install -y \
  build-essential \
  git \
  imagemagick \
  libicu-dev \
  libmagickwand-dev \
  default-libmysqlclient-dev \
  libqt5webkit5-dev \
  libreadline-dev \
  libssl-dev \
  mysql-client \
  nodejs \
  qt5-default \
  xvfb

RUN mkdir /app
WORKDIR /app
COPY . /app
RUN bundle install
ENTRYPOINT rails s --bind 0.0.0.0
